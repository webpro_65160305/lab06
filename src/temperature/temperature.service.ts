import { Injectable } from '@nestjs/common';

@Injectable()
export class TemperatureService {
  convert(celsius: number) {
    return {
      celsius: celsius,
      fahrebheit: (celsius * 9.0) / 5 + 32,
    };
  }
}
